import Vue from "vue";
import Vuex from "vuex";
import api from "../api/routers.js";
import router from "../router/index.js";

Vue.use(Vuex);

const state = () => ({
    data: [],
    user: [],
    routes: {
        register: {
            api: "auth/signup"
        },
        login: {
            api: "auth/login"
        },
    },
});

const getters = {
    User: state => JSON.parse(localStorage.user)
};

const mutations = {

};

const actions = {

    CHECK_ME({ commit }) {
        if(!localStorage.token) {
            // Редирект по имени компонента
            router.push({ name: "Enter" })
        }
    },
    LOG_OUT({commit}){
        alert('Leave')
        localStorage.clear()  
        // Редирект по имени компонента
        router.push({ name: "Enter" })
    },

    REGISTER({ commit }) {
        event.preventDefault()
        let obj = {}
        let fm = new FormData(event.target)

        fm.forEach((value, key) => {
            obj[key] = value
        })
        
        api.post({ api: state().routes.register.api, obj })
        .then(res => {
                if (res.status == 200 || res.status == 201 || res.status == 202) {
                    localStorage.user = JSON.stringify(res.data)
                    localStorage.token = res.data.token

                    router.push({ name: "Home" })
                }
            })
            .catch(err => {
                console.log(err);
            })
    },
    LOG_IN({ commit }) {
        event.preventDefault()
        let obj = {}
        let fm = new FormData(event.target)

        fm.forEach((value, key) => {
            obj[key] = value
        })
        
        api.post({ api: state().routes.register.api, obj })
        .then(res => {
                if (res.status == 200 || res.status == 201 || res.status == 202) {
                    localStorage.user = JSON.stringify(res.data)
                    localStorage.token = res.data.token

                    router.push({ name: "Home" })
                }
            })
            .catch(err => {
                console.log(err);
            })
    }
};

export default {
    state,
    getters,
    mutations,
    actions,
};